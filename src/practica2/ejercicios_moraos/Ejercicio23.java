/*Ejercicio23
 * Programa que calcula el valor de un termino determinado de la sucesion de fibonacci
 * ACM 2019
 */


package ejercicios_moraos;

import java.util.Scanner;

public class Ejercicio23 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner entero = new Scanner (System.in);
		int pos = 0;
		int numant = 0;
		int num = 1;
		int aux = 0;
		
		System.out.println("Introduzca el la posicion del numero de la serie de fibonacci que quiere consultar: ");
		pos = entero.nextInt();
		
		while (pos < 0) {
			System.out.println("Valor no valido. La posicion del numero tiene que ser entero y positivo. Escriba de nuevo la posicion "
					+ "que desea consultar: ");
			pos = entero.nextInt();
		}
		
		if(pos == 0)
			System.out.println("El numero en la posicion 0 es 0");
		else if (pos == 1)
			System.out.println("El numero de la posicion 1 es 1");
		else {
			for (int i = 2; i <= pos; i++) {
				aux = num;
				num += numant;
				numant = aux;
			}
			System.out.println("El numero de la posicion " + pos + " es " + num);
		}

	}

}
