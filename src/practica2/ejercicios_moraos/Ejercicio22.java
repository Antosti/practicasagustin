/*Ejercicio22
 * Programa que calula todos los numeros primos de un intervalo
 * ACM 2019
 */


package ejercicios_moraos;

import java.util.Scanner;

public class Ejercicio22 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		int liminf = 0;
		int limsup = 0;
		int contador = 0;
		
		System.out.println("Introduzca el limite inferior y el limite superior del intervalo: ");
		System.out.print("Limite inferior: ");
		liminf = entero.nextInt();
		System.out.print("Limite superior: ");
		limsup = entero.nextInt();
		
		
		
		while (liminf < 0 || limsup < 0 || limsup < liminf) {
			System.out.println("Error al introducir los limites. Los limites tienen que ser positivos"
					+ " y el limite inferior debe ser menor al superior. Introduzca otra vez el limite inferior y superior: ");
			System.out.print("Limite inferior: ");
			liminf = entero.nextInt();
			System.out.print("Limite superior: ");
			limsup = entero.nextInt();
		}
		
		System.out.println("Los numeros primos en ese intervalo son: ");
		
		
		while (liminf <= limsup)
		{
			int i = 1;
			contador = 0;
			
			if (liminf == 1)
				System.out.println(liminf);
			
			while (i <= liminf) {
				if (liminf % i == 0)
					contador++;
				i++;
			}
			
			if (contador == 2)
				System.out.println(liminf);
			
			liminf++;
		}
		
		

	}

}
