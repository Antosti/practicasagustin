/*Ejercicio21
 * Programa que comprueba si los lados de un triangulo son validos y el tipo de triangulo que crea
 * ACM 2019
 */


package ejercicios_moraos;

import java.util.Scanner;

public class Ejercicio21 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner doble = new Scanner (System.in);
		Scanner caracter = new Scanner (System.in);
		
		double lado1 = 0;
		double lado2 = 0;
		double lado3 = 0;
		Character eleccion = new Character (' ');
		do {
			System.out.println("Introduzca el tama�o de los lados de un triangulo: ");
			System.out.print("Lado 1: ");
			lado1 = doble.nextDouble();
			System.out.print("Lado 2: ");
			lado2 = doble.nextDouble();
			System.out.print("Lado 3: ");
			lado3 = doble.nextDouble();
			
			if ((lado1 + lado2) <= lado3 || (lado1 + lado3) <= lado2 || (lado2 + lado3) <= lado1) {
				System.out.println("Error. Un triangulo no puede tener esos lados.");
			}else {
				
				if (lado1 != lado2 && lado1 != lado3 && lado2 != lado3)
				{
					System.out.println("El triangulo es de tipo escaleno.");
				}else if (lado1 == lado2 && lado2 == lado3)
				{
					System.out.println("El triangulo es equilatero.");
				}else 
				{
					System.out.println("El triangulo es isosceles.");
				}
				
				
				}
			
			System.out.println("Si desea introducir otra vez los lados de un triangulo, pulse 'S'. Si quiere salir pulse N");
			eleccion = caracter.next().charAt(0);
			
			while (!eleccion.equals('s') && !eleccion.equals('S') && !eleccion.equals('n') && !eleccion.equals('N')) {
				System.out.println("Valor no valido. Si desea introducir otra vez los lados de un triangulo, pulse 'S'. Si quiere salir pulse N");
				eleccion = caracter.next().charAt(0);
			}
		}while (eleccion.equals('s') || eleccion.equals('S'));
		
		

	}

}
