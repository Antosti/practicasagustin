/*Ejercicio24
 * Programa que intenta adivinar el numero pensado por el usuario
 * ACM 2019
 */


package ejercicios_moraos;

import javax.swing.JOptionPane;

public class Ejercicio24 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		JOptionPane.showMessageDialog(null, "Piensa un numero del 1 al 10 y el programa intentara adivinarlo. Cuando tengas el numero "
				+ "en mente, pulsa aceptar");
		JOptionPane.showMessageDialog(null, "El numero en el que estas pensando es el numero " + (1 + (int)(Math.random()*10)));

	}

}
