/*Ejercicio15
 * Programa que muestra los multiplos de 3 entre 1 y 3000
 * ACM 2019
 */


package ejercicios_verdes;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner teclado = new Scanner(System.in);
		int menu = 0;
		do {
		System.out.println("Elija el tipo de bucle a emplear para mostrar los multiplos de 3 por su numero:");
		System.out.println("Pulse 1 para bucle for.");
		System.out.println("Pulse 2 para bucle while.");
		System.out.println("Pulse 3 para bucle do while.");
		System.out.println("Pulse 0 para salir.");
		
		menu = teclado.nextInt();
		while (menu != 0 && menu != 1 && menu != 2 && menu != 3)
		{
			System.out.println("Opcion no valida, introduzca la opcion del menu que desee realizar: ");
			System.out.println("Pulse 1 para bucle for.");
			System.out.println("Pulse 2 para bucle while.");
			System.out.println("Pulse 3 para bucle do while.");
			System.out.println("Pulse 0 para salir.");
			menu = teclado.nextInt();
		}
		
		
		if (menu == 1) {
			for (int i = 1; i < 3001; i++) {
				
				if (i % 3 == 0)
				{
					System.out.println(i);
				}
				
			}
		}
		if (menu == 2) {
			int i = 1;
			while(i < 3001) {
				
				if (i % 3 == 0) {
					System.out.println(i);
				}
				i++;
			}
		}
		if (menu == 3) {
			int i = 1;
			do {
				if (i % 3 == 0)
					System.out.println(i);
				
				i++;
			}while (i < 3001);
			}
	
	}while(menu != 0);

}
}
