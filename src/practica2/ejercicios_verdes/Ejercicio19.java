/*Ejercicio19
 * Programa que resuelve ecuaciones de segundo grado
 * ACM 2019
 */


package ejercicios_verdes;

import java.util.Scanner;

public class Ejercicio19 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner doble = new Scanner (System.in);
		double a = 0;
		double b = 0;
		double c = 0;
		double d = 0;
		double x1 = 0;
		double x2 = 0;
		double y = 0;
		double x = 0;
		
		System.out.println("Este programa resuelve ecuaciones de segundo grado. Introduzca los valores de a, b y c de la ecuacion: ");
		a = doble.nextDouble();
		b = doble.nextDouble();
		c = doble.nextDouble();
		
		if (a == 0 && b == 0) {
			System.out.println("Error. La ecuacion no se puede resolver");
		}else if (a == 0 && b !=0) {
			
			x = -c/b;
			System.out.println("La raiz unica es " + x);
			
		}else {
			
			d = Math.pow(b, 2) - (4*a*c);
			if (d > 0) {
				x = (-b/(2*a));
				y = Math.sqrt((Math.pow(b, 2) - (4*a*c))) / (2*a);
				x1 = x + y;
				x2 = x - y;
				System.out.println("Las dos raices reales de la ecuacion son: " + x1 + " y " + x2);
			}else {
				x = -b/(2*a);
				y = Math.sqrt(-(Math.pow(b, 2) - (4*a*c)) ) / (2*a);
				
				System.out.println("Las dos raices complejas de la ecuacion son:\n " + x + " +  " + y + "i\n" + x + " - " + y + "i");
			}
		}

	}

}
