/*Ejercicio11
 * Programa que calcula el inter�s producido y el capital total acumulado de un capital inicial invertido a un tipo de inter�s anual
 * ACM 2019
 */


package ejercicios_verdes;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		Scanner doble = new Scanner (System.in);
		
		double capital = 0;
		double interes = 0;
		int anos = 0;
		double valor = 0;
		double ganancia = 0;
		
		System.out.println("Bienvenido usuario. Introduzca el capital principal inicial: ");
		capital = doble.nextDouble();
		
		System.out.println("Introduzca ahora el tipo de interes anual en porcentaje (solo valor numerico): ");
		interes = doble.nextDouble() / 100;
		
		System.out.println("Para finalizar, introduzca el numero de a�os que desea calcular con estos datos: ");
		anos = entero.nextInt();
		
		valor = capital * Math.pow(1 + interes, anos);
		ganancia = valor - capital;
		
		System.out.println("Su capital final tras " + anos + " a�os es: " + valor);
		System.out.println("Sus ganancias con ese tipo de interes en total son " + ganancia);
		
	}

}
