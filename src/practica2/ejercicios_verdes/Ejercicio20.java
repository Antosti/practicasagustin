/*Ejercicio20
 * Programa con el que puedes realizar suma, resta, multiplicacion y division a eleccion del usuario
 * ACM 2019
 */


package ejercicios_verdes;

import java.util.Scanner;

public class Ejercicio20 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner entero = new Scanner (System.in);
		Scanner doble = new Scanner (System.in);
		int menu = 0;
		double num1 = 0;
		double num2 = 0;
		double resultado = 0;
		
		do {
			System.out.println("Elija que operacion quiere realizar:");
			System.out.println("Sumar dos numeros : 1");
			System.out.println("Restar dos numeros : 2");
			System.out.println("Multiplicar dos numeros : 3");
			System.out.println("Dividir dos numeros : 4");
			System.out.println("Para salir: 0");
			
			menu = entero.nextInt();
			while (menu != 0 && menu != 1 && menu != 2 && menu != 3 && menu != 4)
			{
				System.out.println("Opcion no valida, introduzca la opcion del menu que desee realizar: ");
				System.out.println("Elija que operacion quiere realizar:");
				System.out.println("Sumar dos numeros : 1");
				System.out.println("Restar dos numeros : 2");
				System.out.println("Multiplicar dos numeros : 3");
				System.out.println("Dividir dos numeros : 4");
				System.out.println("Para salir: 0");
				menu = entero.nextInt();
			}
			
			if (menu == 1) {
				System.out.println("Introduzca dos numeros para sumar: ");
				num1 = entero.nextDouble();
				num2 = entero.nextDouble();
				
				resultado = num1 + num2;
				
				System.out.println("El resultado de la suma es " + resultado);
				
			}
			if (menu == 2) {
				System.out.println("Introduzca dos numeros para restar (el segundo restara al primero): ");
				num1 = entero.nextInt();
				num2 = entero.nextInt();
				
				resultado = num1 - num2;
				
				System.out.println("El resultado de la suma es " + resultado);
				
			}
			if (menu == 3) {
				System.out.println("Introduzca dos numeros para multiplicar: ");
				num1 = entero.nextInt();
				num2 = entero.nextInt();
				
				resultado = num1 * num2;
				
				System.out.println("El resultado de la suma es " + resultado);
				
			}
			if (menu == 4) {
				System.out.println("Introduzca dos numeros para dividir (el segundo dividira al primero): ");
				num1 = entero.nextInt();
				num2 = entero.nextInt();
				
				resultado = num1 / num2;
				
				System.out.println("El resultado de la suma es " + resultado);
				
			}
			
		}while (menu != 0);

	}

}
