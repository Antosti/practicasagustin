/*Ejercicio9
 * Programa que muestra por pantalla 3 letras dibujadas
 * ACM 2019
 */


package ejercicios_verdes;

public class Ejercicio9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("" +
				"                                A A     CCCCCCC  M     M\r\n" + 
				"                               A   A    C        MM   MM\r\n" + 
				"                              A     A   C        M M M M\r\n" + 
				"                              AAAAAAA   C        M  M  M\r\n" + 
				"                              A     A   C        M     M\r\n" + 
				"                              A     A   C        M     M\r\n" + 
				"                              A     A   CCCCCCC  M     M");
			
	}

}
