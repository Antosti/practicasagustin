/*Ejercicio13
 * Programa que lee numeros por consola y al finalizar, muestra el mayor numero introducido
 * ACM 2019
 */


package ejercicios_verdes;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		Scanner caracter = new Scanner (System.in);
		Character menu = new Character (' ');
		int num = 0;
		int mayor = 0;
		
		do {
			System.out.println("Introduzca un numero entero: ");
			num = entero.nextInt();
			
			if(num > mayor)
				mayor = num;
			
			System.out.println("Si quiere introducir otro numero, pulse la tecla S. En caso contrario, pulse cualquier otra tecla: ");
			menu = caracter.next().charAt(0);
			
		}while (menu.equals('S') || menu.equals('s'));
		System.out.println("El numero mayor que has introducido es el: " + mayor);
		
	}

}
