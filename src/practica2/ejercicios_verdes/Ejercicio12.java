/*Ejercicio 12
 * Programa que suma 8 numeros enteros y los muestra por pantalla
 * ACM 2109
 */


package ejercicios_verdes;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner entero = new Scanner (System.in);
		int suma = 0;
		
		System.out.println("Escriba 8 numeros enteros: ");
		for (int i = 0; i < 8; i++) {
			suma += entero.nextInt();
		}
		
		System.out.println("La suma de sus 8 numeros es: " + suma);
	}

}
