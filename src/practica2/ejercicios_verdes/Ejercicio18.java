/*Ejercicio18
 * Programa que calcula el factorial de un numero introducido por pantalla
 * ACM 2019
 */


package ejercicios_verdes;

import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		int num = 0;
		int factorial = 1;
		
		System.out.println("Introduzca un numero mayor o igual que 0: ");
		num = entero.nextInt();
		while(num < 0) {
			System.out.println("Valor no valido, introduzca un numero mayor o igual que 0: ");
			num = entero.nextInt();
		}
		if (num == 0)
			System.out.println("El factorial de 0 es 1");
		else {
			factorial = num;
			for (int i = num -1; i > 0; i--)
			{
				factorial = factorial * i;
			}
			System.out.println("El resultado del factorial del numero " + num + " es: " + factorial);
		}

	}

}
