/*Ejercicio14
 * Programa que lee numeros enteros y cuando acaba, muestra la suma de todos los numeros leidos y su media aritmetica
 * ACM 2019
 */


package ejercicios_verdes;

import java.util.Scanner;

public class Ejercicio14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner entero = new Scanner (System.in);
		
		int contador = 0;
		double suma = 0;
		double menu = 0;
		double media = 0;
		System.out.println("Escriba todos los numeros enteros positivos que desee. Cuando quiera salir, pulse 0.");
		do {
			
			menu = entero.nextDouble();
			while(menu < 0) {
				System.out.println("Valor no valido, introduzca un numero entero positivo: ");
				menu = entero.nextInt();
			}
			suma += menu;
			contador ++;
			
		}while (menu != 0);
		 
		media = (suma/(contador-1));
		System.out.println("La suma total de los enteros introducidos es de: " + suma);
		System.out.println("La media aritmetica de la suma es: "+ media);
		

	}

}
