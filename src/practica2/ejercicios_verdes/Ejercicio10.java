/*Ejercicio10
 * Programa que pide 3 numeros enteros por y los muestra por pantalla
 * ACM 2019
 */

package ejercicios_verdes;
import java.util.Scanner; 

public class Ejercicio10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner teclado = new Scanner(System.in);
		
		int num1;
		int num2;
		int num3;
		int pos1;
		int pos2;
		int pos3;
		
		System.out.println("Escribe 3 numeros enteros: ");
		num1 = teclado.nextInt();
		num2 = teclado.nextInt();
		num3 = teclado.nextInt();
		pos1 = num1;
		pos2 = num2;
		pos3 = num3;
		
		System.out.println("Los numeros ordenados: ");
		if (num2 < num1) {
			pos1 = num2;
			pos2 = num1;
		}
		if (num3 < pos1) {
			pos3 = pos2;
			pos2 = pos1;
			pos1 = num3;
		}
		if (pos3 < pos2 && pos3 >= pos1)
		{
			pos3 = pos2;
			pos2 = num3;
		}
		
		System.out.println(pos1 + " " + pos2 + " " + pos3);
	}
}