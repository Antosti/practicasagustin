/*Ejercicio 17
 * Programa que dibuja un cuadrado de asteriscos del tama�o que el usuario quiera
 * ACM 2019
 */


package ejercicios_verdes;

import java.util.Scanner;

public class Ejercicio17 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner entero = new Scanner (System.in);
		int num = 0;
		
		System.out.println("Introduzca de que tama�o quiere el cuadrado: ");
		num = entero.nextInt();
		
		System.out.println("El cuadrado de tama�o " + num + " queda asi:");
		
		for (int i = 0; i < num; i++)
		{
			System.out.print("*");
		}
		System.out.println();
		
		for (int i = 0; i < num -2; i++)
		{
			System.out.print("*");
			for (int j = 0; j < num -2; j++)
			{
				System.out.print(" ");
			}
			System.out.println("*");
		}
		
		for (int i = 0; i < num; i++)
		{
			System.out.print("*");
		}

	}

}
