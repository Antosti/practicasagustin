/*Ejercicio 16
 * Programa que redondea un numero con decimales
 * ACM 2019
 */


package ejercicios_verdes;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner teclado = new Scanner (System.in);
		Scanner entero = new Scanner (System.in);
		
		int menu = 0;
		double num = 0;
		int decimal = 0;
		double valor = 0;
		
		do {
			System.out.println("Bienvenido. Introduzca el numero a redondear con varios decimales: ");
			num = teclado.nextDouble();
			
			System.out.println("Cuantos decimales desea que tenga el numero?: ");
			decimal = entero.nextInt();	
			
			valor = Math.rint(num * Math.pow(10, decimal))/ Math.pow(10, decimal);
			
			System.out.println("El numero redondeado es: \n" + valor);
			System.out.println("Si desea redondear otro numero, pulse 1. Si desea salir del programa pulse 0: ");
			menu = entero.nextInt();			
		}while(menu != 0);

	}

}
