import java.util.Scanner;
import java.util.Vector;

public class ordenacionBaraja{
	
	static Vector <Integer> numeros = new Vector <Integer>();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		int a = 0;
		int sol = 0;
		System.out.println("Introduzca los valores del vector de enteros. Pulse 100 dejar de meter numeros: ");
		do
		{
			a = entero.nextInt();
			if(a == 100)
				break;
			numeros.add(a);
		}while(a != 100);
		
		sol = ordenarBaraja(numeros);
		if(sol == 1) {
			System.out.println("El vector se ha ordenado de la siguiente forma: ");
			for (int i = 0; i < numeros.size();i++)
				System.out.println(numeros.get(i) + " ");
		}
		
		if (sol == 0)
			System.out.println("El vector estaba ya ordenado.");
		
		if(sol == -1)
			System.out.println("El vector no se ha podido ordenar.");

	}
	
	static int ordenarBaraja(Vector <Integer> v) {
		int sol = 0;
		int anterior = 0;
		int actual = 0;
		int inicio = 1;
		int aux = 0;
		int i = 0;
		boolean cambios = false;
		
		
		for(int j = 1; j < v.size(); j++) {
			i = j -1;
			aux = v.get(j);
			while(i >= 0 && v.get(i) > aux) {
				anterior = v.get(i);
				v.set(i+1, anterior);
				cambios = true;
				i--;
			}
			if(i == -1)
				v.set(0, aux);
			else
				v.set(i+1, aux);
			
				
					
			
			
		}		
		
		if (cambios)
			sol = 1;
		
		for(int i1 = 0; i1 < numeros.size()-1; i1++) {
			anterior = v.get(i1);
			actual= v.get(i1+1);
			if (anterior > actual)
				sol = -1;
		}
		
		return sol;
	}
	
}
