import java.util.Scanner;
import java.util.Vector;

public class Indice {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Vector <Integer> solucion = new Vector <Integer> ();
		Scanner caracter = new Scanner(System.in);
		String palabra = "";
		System.out.println("Introduzca el texto que desee: ");
		palabra = caracter.nextLine();
		solucion = indiceAlfabetico(palabra);
		
		System.out.println("La posicion de las letras en el alfabeto es: (sin numeros ni caracteres especiales y contando el 0 como espacio)");
		for (int i = 0; i < solucion.size(); i++)
			System.out.print(solucion.get(i) + " ");

	}
	
	static Vector <Integer> indiceAlfabetico(String p){
		char[] alfabeto = {' ','a','b','c','d','e','f','g','h','i','j','k','l','m','n','�','o','p','q','r','s','t','u','v','w','x','y','z'};
		Vector <Integer> solucion = new Vector <Integer>();
		String palabra = p.toLowerCase();
		char[] pal = palabra.toCharArray();
		for(int i = 0; i < pal.length; i++)
			for (int j = 0; j < alfabeto.length; j++) 
				if (pal[i] == alfabeto[j]) 
					solucion.add(j);
			
		return solucion;
	}

}
