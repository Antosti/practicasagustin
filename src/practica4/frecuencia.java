import java.util.Scanner;
import java.util.Vector;

public class frecuencia {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		int a = 0;
		int num = 0;
		int frecuencia = 0;
		Vector <Integer> numeros = new Vector <Integer>();
		
		System.out.println("Escriba los numeros que desee introducir en el vector. "
				+ "Introduzca 100 para salir: ");
		do
		{
			a = entero.nextInt();
			if(a == 100)
				break;
			numeros.add(a);
		}while(a != 100);
		
		System.out.println("Introduzca el numero del que quiere saber la frecuencia: ");
		num = entero.nextInt();
		frecuencia = frecuenciaNumero(numeros, num);
		System.out.println("La frecuencia del numero "+ num + " es " + frecuencia);
 
	}
	
	static int frecuenciaNumero(Vector v, int num)
	{
		int contador = 0;
		for (int i = 0; i < v.size(); i++)
			if((int)v.get(i) == num)
				contador++;
		return contador;
	}

}
