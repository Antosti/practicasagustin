import java.util.Scanner;
import java.util.Vector;

public class ordenacionBurbuja {

	static Vector <Integer> numeros = new Vector <Integer>();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		int a = 0;
		int sol = 0;
		System.out.println("Introduzca los valores del vector de enteros. Pulse 100 dejar de meter numeros: ");
		do
		{
			a = entero.nextInt();
			if(a == 100)
				break;
			numeros.add(a);
		}while(a != 100);
		
		sol = ordenarBurbuja(numeros);
		if(sol == 1) {
			System.out.println("El vector se ha ordenado de la siguiente forma: ");
			for (int i = 0; i < numeros.size();i++)
				System.out.println(numeros.get(i) + " ");
		}
		
		if (sol == 0)
			System.out.println("El vector estaba ya ordenado.");
		
		if(sol == -1)
			System.out.println("El vector no se ha podido ordenar.");

	}
	static int ordenarBurbuja(Vector <Integer> v) {
		int a = 0;
		int b = 0;
		int sol = 0;
		int cambios = 0;
		for(int i = 0; i < v.size() - 1; i++)
			for (int j = 0; j < v.size()-1; j++) {
				a = v.get(j);
				b = v.get(j+1);
				if(a > b)
				{
					cambios++;
					v.set(j+1, a); 
					v.set(j, b);
				}
			}
		if (cambios != 0)
			sol = 1;
		
		for(int i = 0; i < numeros.size()-1; i++) {
			a = v.get(i);
			b = v.get(i+1);
			if (a > b)
				sol = -1;
		}
		
		
		return sol;
		
	}
}
