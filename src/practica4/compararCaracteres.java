import java.util.Scanner;
import java.util.Vector;

public class compararCaracteres {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner letras = new Scanner (System.in);
		String a = "";
		String b = "";
		char[] car1;
		char[] car2;
		System.out.println("Introduzca un texto para comparar: ");
		a = letras.nextLine();
		System.out.println("Introduzca el segundo texto para comparar: ");
		b = letras.nextLine();
		car1 = a.toCharArray();
		car2 = b.toCharArray();
		
		switch(compararVectoresChar(car1, car2)) {
		case 0:
			System.out.println("Los textos son iguales.");
			break;
		case 1:
			System.out.println("El texto 1 es mayor que el texto 2.");
			break;
		case -1:
			System.out.println("El texto 2 es mayor que el texto 1");
			break;
		}

	}
	
	static int compararVectoresChar (char[] a1, char[] a2)
	{
		int sol = 0;
		if (a1.length > a2.length)
			sol = 1;
		else if (a2.length > a1.length)
			sol = -1;
		else
			for (int i = 0; i < a1.length || i < a2.length; i++){
				if (a1[i] > a2[i]) {
					sol = 1;
					break;
				}
				if (a2[i] > a1[i]) {
					sol = -1;
					break;
				}
				
			}
		return sol;
	}

}
