import java.util.Scanner;

public class matrizmaxima {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		int fila = 0;
		int columna = 0;
		int [][] matriz;
		
		System.out.println("Introduzca los valores del numero de filas y columnas de la matriz: ");
		System.out.print("Filas: ");
		fila = entero.nextInt();
		System.out.print("Columnas: ");
		columna = entero.nextInt();
		while(fila < 3 || columna < 3) {
			System.out.println("Valor no valido, la matriz tiene que ser al menos de 3x3. Introduzca datos validos: ");
			System.out.print("Filas: ");
			fila = entero.nextInt();
			System.out.print("Columnas: ");
			columna = entero.nextInt();
		}
		matriz = new int [fila][columna];
		System.out.println("Introduzca los datos de la matriz. De arriba a abajo, de izquierda a derecha: ");
		for (int i = 0; i < fila; i++) {
			System.out.println("Columna " + i + ":");
			for (int j = 0; j < columna; j++) {
				matriz[i][j] = entero.nextInt();
				}
		}
		matriz = maxima3(matriz);
		
		
		System.out.println("Submatriz maxima 3:");
		for(int j=0; j<3; j++)
	    {
	        for(int i=0; i<3; i++)
	        {
	            System.out.print(matriz[i][j]+" ");
	            
	        }
	        System.out.println();
	    }
		

	}
	
	static int[][] maxima3(int[][] m){
		int [][] matriz = new  int[3][3];
		int maxSuma = Integer.MIN_VALUE;
		int mejorFila = 0;
		int mejorColum = 0;
		for (int fila = 0; fila < m.length-2; fila++) {
			for (int colum = 0; colum < m[1].length-2; colum ++){
				int suma = m[fila][colum] + m[fila][colum + 1] + m[fila][colum +2]+ m[fila + 1][colum]+ m[fila + 1][colum + 1] + m[fila +1]
						[colum +2] + m[fila+2][colum] + m[fila +2][colum +1] + m[fila +2][colum + 2];
				if (suma > maxSuma) {
					maxSuma = suma;
					mejorFila = fila;
					mejorColum = colum;
				}
		}

		}
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++) {
				matriz[i][j] = m[mejorFila + i][mejorColum + j];
			}
		
		return matriz ;
	}
}
