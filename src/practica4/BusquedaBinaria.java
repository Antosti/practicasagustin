import java.util.Scanner;
import java.util.Vector;

public class BusquedaBinaria {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		int[] numeros = new int[] {1,2,5,8,9,14,16,17,19,20,21,22,25};
		int num = 0;
		int sol = 0;
		System.out.println("Introduce el numero que desea buscar: ");
		num = entero.nextInt();
		sol = buscarNumero(numeros, num);
		if (sol == -1)
			System.out.println("El numero que busca no se encuentra en el vector.");
		else
			System.out.println("El numero " + num + " se encuentra en la posicion " + sol + " del vector.");
		

	}
	
	static int buscarNumero(int[] n, int num) {
		int inf = 0;
		int sup = n.length -1;
		int sol = 0;
		
		do {
			int centro = (sup + inf)/2;
			if(n[centro] == num)
				sol = centro;
			if(num < n[centro])
				sup = centro -1;
			if (num > n[centro])
				inf = centro +1;
			if(num < n[inf] || num > n[sup])
				sol = -1;
			
		}while (sol == 0);
		
		return sol;
	}

}
