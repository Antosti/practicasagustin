/*Abecedario.java
 * Programa que dibuja letras de un texto introducido
 * ACM 2019
 */

import java.util.Scanner;

public class BannerLetras {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner letras = new Scanner (System.in);
		String palabra = "";
		char[] letra;
		
		System.out.println("Escriba la letra o letras que quieres que el programa muestre: ");
		palabra = letras.nextLine();
		palabra = palabra.toUpperCase();
		letra = palabra.toCharArray();
		for (int i = 0; i < letra.length; i++)
			System.out.println(bannerLetra(letra[i])+ "\n");
		

	}
	
	static String bannerLetra(char c) {
		
		String letra = "";
		switch (c) {
		case 'A':
			 	letra += "  A A\n"+			
			 			 " A   A \n"+  
			 			 "A     A  \n"+
			 			 "AAAAAAA  \n"+
			 			 "A     A  \n"+
			 			 "A     A  \n"+
			 			 "A     A \n";
			break;
		case 'B':
			letra += "BBBBBBB\n"+			
		 			 "B     B\n"+  
		 			 "B     B  \n"+
		 			 "BBBBBB  \n"+
		 			 "B     B   \n"+
		 			 "B     B  \n"+
		 			 "BBBBBBB ";
			break;
		case 'C':
			letra += "CCCCCCC\n"+			
		 			 "C       \n"+  
		 			 "C       \n"+
		 			 "C       \n"+
		 			 "C       \n"+
		 			 "C       \n"+
		 			 "CCCCCCC ";
			break;
		case 'D':
			letra += "DDDD \n"+			
		 			 "D   D \n"+  
		 			 "D    D  \n"+
		 			 "D     D  \n"+
		 			 "D    D   \n"+
		 			 "D   D    \n"+
		 			 "DDDD ";
			break;
		case 'E':
			letra += "EEEEEEE\n"+			
		 			 "E       \n"+  
		 			 "E       \n"+
		 			 "EEEEE  \n"+
		 			 "E       \n"+
		 			 "E       \n"+
		 			 "EEEEEEE ";
			break;
		case 'F':
			letra += "FFFFFFF\n"+			
		 			 "F      \n"+  
		 			 "F      \n"+
		 			 "FFFFF  \n"+
		 			 "F       \n"+
		 			 "F      	\n"+
		 			 "F      ";
			break;
		case 'G':
			letra += "GGGGGGG\n"+			
		 			 "G     G\n"+  
		 			 "G       \n"+
		 			 "G  GGGG  \n"+
		 			 "G     G  \n"+
		 			 "G     G  \n"+
		 			 "GGGGGGG ";
			break;
		case 'H':
			letra += "H     H\n"+			
		 			 "H     H\n"+  
		 			 "H     H  \n"+
		 			 "HHHHHHH  \n"+
		 			 "H     H  \n"+
		 			 "H     H  \n"+
		 			 "H     H ";
			break;
		case 'I':
			letra += "IIIIIII\n"+			
		 			 "   I  \n"+  
		 			 "   I     \n"+
		 			 "   I   \n"+
		 			 "   I   \n"+
		 			 "   I  \n"+
		 			 "IIIIIII ";
			break;
		case 'J':
			letra += "JJJJJJJ\n"+			
		 			 "      J\n"+  
		 			 "      J  \n"+
		 			 "      J  \n"+
		 			 "JJJ   J  \n"+
		 			 "J     J  \n"+
		 			 "JJJJJJJ ";
			break;
		case 'K':
			letra += "K     K\n"+			
		 			 "K    K \n"+  
		 			 "K   K  \n"+
		 			 "KKKK   \n"+
		 			 "K   K  \n"+
		 			 "K    K \n"+
		 			 "K     K";
			break;
		case 'L':
			letra += "L\n"+			
		 			 "L\n"+  
		 			 "L\n"+
		 			 "L\n"+
		 			 "L\n"+
		 			 "L\n"+
		 			 "LLLLLLL";
			break;
		case 'M':
			letra += "M     M\n"+			
		 			 "MM   MM\n"+  
		 			 "M M M M  \n"+
		 			 "M  M  M  \n"+
		 			 "M     M  \n"+
		 			 "M     M  \n"+
		 			 "M     M ";
			break;
		case 'N':
			letra += "N     N\n"+			
		 			 "NN    N\n"+  
		 			 "N N   N  \n"+
		 			 "N  N  N  \n"+
		 			 "N   N N  \n"+
		 			 "N    NN  \n"+
		 			 "N     N ";
			break;
		case '�':
			letra += "�������\n"+			
		 			 "\n"+  
		 			 "��    �  \n"+
		 			 "� �   �  \n"+
		 			 "�  �  �  \n"+
		 			 "�   � �  \n"+
		 			 "�    ��";
			break;
		case 'O':
			letra += "OOOOOOO\n"+			
		 			 "O     O\n"+  
		 			 "O     O  \n"+
		 			 "O     O  \n"+
		 			 "O     O \n"+
		 			 "O     O \n"+
		 			 "OOOOOOO ";
			break;
		case 'P':
			letra += "PPPPPPP\n"+			
		 			 "P     P\n"+  
		 			 "P     P  \n"+
		 			 "PPPPPPP  \n"+
		 			 "P       \n"+
		 			 "P       \n"+
		 			 "P      \n";
			break;
		case 'Q':
			letra += "QQQQQQ \n"+			
		 			 "Q    Q \n"+  
		 			 "Q    Q \n"+
		 			 "Q  Q Q \n"+
		 			 "Q   QQ \n"+
		 			 "QQQQQQ \n"+
		 			 "      Q ";
			break;
		case 'R':
			letra += "RRRRRRR\n"+			
		 			 "R     R\n"+  
		 			 "R     R  \n"+
		 			 "RRRRRRR  \n"+
		 			 "R   R    \n"+
		 			 "R    R   \n"+
		 			 "R     R  ";
			break;
		case 'S':
			letra += "SSSSSSS  \n"+			
		 			 "S     \n"+  
		 			 "S        \n"+
		 			 "SSSSSSS  \n"+
		 			 "      S  \n"+
		 			 "      S  \n"+
		 			 "SSSSSSS ";
			break;
		case 'T':
			letra += "TTTTTTT\n"+			
		 			 "   T \n"+  
		 			 "   T   \n"+
		 			 "   T  \n"+
		 			 "   T \n"+
		 			 "   T   \n"+
		 			 "   T   ";
			break;
		case 'U':
			letra += "U     U\n"+			
		 			 "U     U\n"+  
		 			 "U     U\n"+
		 			 "U     U\n"+
		 			 "U     U\n"+
		 			 "U     U\n"+
		 			 "UUUUUUU";
			break;
		case 'V':
			letra += "V     V\n"+			
		 			 "V     V\n"+  
		 			 "V     V\n"+
		 			 "V     V\n"+
		 			 " V   V\n"+
		 			 "  V V\n"+
		 			 "   V";
			break;
		case 'W':
			letra += "W     W\n"+			
		 			 "W     W\n"+  
		 			 "W     W\n"+
		 			 "W  W  W\n"+
		 			 "W W W W\n"+
		 			 "WW   WW\n"+
		 			 "W     W";
			break;
		case 'X':
			letra += "X     X"+			
		 			 " X   X "+  
		 			 "  X X  "+
		 			 "   X  "+
		 			 "  X X   \n"+
		 			 " X   X  \n"+
		 			 "X     X";
			break;
		case 'Y':
			letra += "Y     Y\n"+			
		 			 " Y   Y \n"+  
		 			 "  Y Y  \n"+
		 			 "   Y  \n"+
		 			 "   Y   \n"+
		 			 "   Y   \n"+
		 			 "   Y    ";
			break;
		case 'Z':
			letra += "ZZZZZZZ\n"+			
		 			 "     Z \n"+  
		 			 "    Z   \n"+
		 			 "   Z  \n"+
		 			 "  Z     \n"+
		 			 " Z      \n"+
		 			 "ZZZZZZZ ";
			break;
		default:
			letra += "\n\n\n\n\n\n\n";
		}
		
		return letra;
	}

}
