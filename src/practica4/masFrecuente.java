import java.util.Scanner;
import java.util.Vector;

public class masFrecuente {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		int a = 0;
		int num = 0;
		Vector <Integer> numeros = new Vector <Integer>();
		
		System.out.println("Escriba los numeros que desee introducir en el vector. "
				+ "Introduzca 100 para salir: ");
		do
		{
			a = entero.nextInt();
			if(a == 100)
				break;
			numeros.add(a);
		}while(a != 100);
		System.out.println("El numero " + masFrecuenteInt(numeros) + " es el mas frecuente en el vector.");

	}
	
	static int masFrecuenteInt(Vector v) {
		int sol = 0;
		int a = 0;
		int contador = 0;
		int aux2 = 0;
		
		for (int i = 0; i < v.size(); i++) {
			contador = 0;
			a = (int)v.get(i);
			for (int j = 0; j < v.size(); j++) {
				if (a == (int)v.get(j))
					contador++;
			}
			if (contador > aux2) {
				aux2 = contador;
				sol = a;
			}
		}
		return sol;
	}

}
