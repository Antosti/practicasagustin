import java.util.Scanner;
import java.util.Vector;

public class vectorOrdenado {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		int a = 0;
		int num = 0;
		int frecuencia = 0;
		Vector <Integer> numeros = new Vector <Integer>();
		
		System.out.println("Escriba los numeros que desee introducir en el vector. "
				+ "Introduzca 100 para salir: ");
		do
		{
			a = entero.nextInt();
			if(a == 100)
				break;
			numeros.add(a);
		}while(a != 100);
		if(yaOrdenadoInt(numeros))
			System.out.println("El vector esta ordenado");
		else
			System.out.println("El vector esta desordenado.");

	}
	
	static boolean yaOrdenadoInt(Vector <Integer> v) {
		int a = 0;
		int b = 0;		
		boolean sol = true;
		for (int i = 0; i < v.size()-1; i++)
		{
			a = (int)v.get(i);
			b = (int)v.get(i+1);
			if (a+1 != b) {
				sol = false;
				break;
			}
		}
		return sol;
	}
}
