import java.util.Scanner;
import java.util.Vector;

public class contarConsecutivos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		int a = 0;
		int b = 0;
		Vector <Integer> numeros = new Vector <Integer>();
		
		System.out.println("Escriba los numeros que desee introducir en el vector. "
				+ "Introduzca 100 para salir: ");
		do
		{
			a = entero.nextInt();
			if(a == 100)
				break;
			numeros.add(a);
		}while(a != 100);
		b = contarIntConsecutivos(numeros);
		if (b == 1)
			System.out.println("No hay numeros consecutivos en el vector.");
		else
			System.out.println("Hay "+ b + " numeros consecutivos en el vector.");

	}
	
	static int contarIntConsecutivos(Vector <Integer> v) {
		int max = 0;
		int contador = 1;
		int a = 0;
		int b = 0;
		for (int i = 1; i < v.size(); i++)
		{
			a = (int)v.get(i);
			b = (int)v.get(i-1);
			if(a == b + 1)
				contador++;
			else {
				if (contador > max)
					max = contador;
				contador = 1;
			}
			
		}
		if (contador > max)
			max = contador;
		return max;
	}
}
