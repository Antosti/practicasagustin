import java.util.Scanner;

public class matrices {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		int [][] matrizA;
		int [][] matrizB;
		int [][] matrizC;
		int [][] matrizD;
		int num = 0;
		System.out.println("Introduzca el tama�o del lado del cuadrado: ");
		num = entero.nextInt();
		matrizA = secuenciaNaturalIntA(num);
		matrizB = secuenciaNaturalIntB(num);
		matrizC = secuenciaNaturalIntC(num);
		matrizD = secuenciaNaturalIntD(num);
		
		System.out.println("Matriz A:");
		for(int j=0; j<num; j++)
	    {
	        for(int i=0; i<num; i++)
	        {
	            System.out.print(matrizA[i][j]+" ");
	            
	        }
	        System.out.println();
	    }
		
		System.out.println("Matriz B:");
		for(int j=0; j<num; j++)
	    {
	        for(int i=0; i<num; i++)
	        {
	            System.out.print(matrizB[i][j]+" ");
	        }
	        System.out.println();
	    }
		
		System.out.println("Matriz C:");
		for(int j=0; j<num; j++)
	    {
	        for(int i=0; i<num; i++)
	        {
	            System.out.print(matrizC[i][j]+" ");
	        }
	        System.out.println();
	    }
		System.out.println("Matriz D:");
		for(int j=0; j<num; j++)
	    {
	        for(int i=0; i<num; i++)
	        {
	            System.out.print(matrizD[i][j]+" ");
	        }
	        System.out.println();
	    }
		
		
		

	}
	
	static int[][] secuenciaNaturalIntA(int num) {
		int [][] matriz = new int [num][num];
		int valores = 1;
		for(int i = 0; i < num; i ++)
			for (int j = 0; j < num; j++) {
				matriz[i][j] = valores;
				valores++;
			}
		return matriz;
		
	}
	static int[][] secuenciaNaturalIntB(int num) {
		int [][] matriz = new int [num][num];
		int valores = 1;
		for(int i = 0; i < num; i ++)
			if(i%2 == 0)
				for (int j = 0; j < num; j++) {
					matriz[i][j] = valores;
					valores++;
				}
			else
				for (int j = num-1; j >= 0; j--) {
					matriz[i][j] = valores;
					valores++;
				}
				
				
		return matriz;
		}

	static int[][] secuenciaNaturalIntC(int num) {
		int [][] matriz = new int [num][num];
		int valores = 1;
		int fila = num -1;
		int columna = 0;
		int ultimaColumnaInicio = 0;
		int ultimaFilaInicio = num - 1;
		int Finalfila = num -1;

		do {
		      matriz [columna][fila] = valores;
		      if (fila == num -1 && ultimaFilaInicio > 0) {
		    	  columna = 0;
		    	  fila = ultimaFilaInicio -1;
		    	  ultimaFilaInicio = fila;
		    	   }
		    	  
		      else if (columna == num -1 && fila == Finalfila) {
		    	  columna = ultimaColumnaInicio +1;
		    	  fila = 0;
		    	  ultimaColumnaInicio = columna;
		    	  Finalfila -= 1;
		      }else {
		    	  fila++;
		    	  columna++;
		      }
		      valores++;
		    	  
		    } while ((columna != num));
		
		return matriz;
	}
	
	static int[][] secuenciaNaturalIntD(int num) {
		int a = 0;
		int b = num -1;
		int valores = 1;
		int [][] matriz = new int [num][num];
		
		for(int i = 0; i < matriz.length; i++) {
			
			for(int j = a; j <=b; j++) {
				matriz [a][j] = valores;
				valores++;
			}
			for(int j = a+1; j <=b; j++) {
				matriz [j][b] = valores;
				valores++;
			}
			for(int j = b-1; j >=a; j--) {
				matriz [b][j] = valores;
				valores++;
			}
			for(int j = b-1; j >=a+1; j--) {
				matriz [j][a] = valores;
				valores++;
			}
			a++;
			b--;
		}
		return matriz;
		
	}


}
