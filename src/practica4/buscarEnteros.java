import java.util.Scanner;
import java.util.Vector;

public class buscarEnteros {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		int a = 0;
		int num = 0;
		int num2 = 0;
		int frecuencia = 0;
		Vector <Integer> numeros = new Vector <Integer>();
		System.out.println("Escriba los numeros que desee introducir en el vector. "
				+ "Introduzca 100 para salir: ");
		do
		{
			a = entero.nextInt();
			if(a == 100)
				break;
			numeros.add(a);
		}while(a != 100);
		
		System.out.println("Escriba el numero que desee buscar en el vector: ");
		num = entero.nextInt();
		num2 = buscarInt(numeros, num);
		if (num2 == -1)
			System.out.println("El numero " + num + " no esta en el vector");
		else
			System.out.println("El numero "+ num + " esta en la posicion " + num2);

	}
	static int buscarInt(Vector <Integer> v, int n) {
		int a = 0;
		int i;
		int pos = 0;
		for(i = 0; i < v.size(); i++) {
			a = (int)v.get(i);
			if(a == n) {
				pos = i;
				break;
			}
		}
		return pos;
			
	}

}
