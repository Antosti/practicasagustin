import java.util.Scanner;

public class RellenarStringCaracteres {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner cadena = new Scanner(System.in);
		Scanner caracter = new Scanner(System.in);
		Scanner numero = new Scanner(System.in);
		
		String texto = "";
		char c = ' ';
		int num = 0;
		
		System.out.println("Introduzca una palabra o texto: ");
		texto = cadena.nextLine();
		
		System.out.println("Introduzca un caracter: ");
		c = caracter.nextLine().charAt(0);
		
		System.out.println("Introduzca la cantidad de caracteres de la palabra y el caracter introducido: ");
		num = numero.nextInt();
		
		System.out.println(padRight(texto, c, num));

	}
	
	public static StringBuilder padRight(String cadena, char caracter, int num) {
		
		StringBuilder solucion = new StringBuilder ();
		
		solucion.append(cadena);
		
		if (num > cadena.length()) {
			
			num = num - cadena.length();
			
			for(int i = 0; i < num; i++)
				solucion.append(caracter);
		}

		return solucion;
	}

}
