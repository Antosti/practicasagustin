import java.util.Scanner;

public class Mayusculas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner cadena = new Scanner(System.in);
		
		System.out.println("Escriba un texto. Escriba entre las etiquetas <mayus> y </mayus> las partes que "
				+ "quiere que el programa ponga en mayusculas: ");
		StringBuilder texto = new StringBuilder(cadena.nextLine());
		
		System.out.println(pasarMayusculaSubCadena(texto));

	}
	
	public static StringBuilder pasarMayusculaSubCadena(StringBuilder cadena) {
		
		int ind1 = 0;
		int ind2 = 0;
		String etiqueta1 = "<mayus>";
		String etiqueta2 = "</mayus>";
		String solucion = "";
		
		
		while(cadena.indexOf(etiqueta1) > -1) {
			ind1 = cadena.indexOf(etiqueta1);
			ind2 = cadena.indexOf(etiqueta2);
			solucion = cadena.substring(ind1 + etiqueta1.length(), ind2);
			cadena.replace(ind1 , ind2 + etiqueta2.length(), solucion.toUpperCase());
		}
	
		
		return cadena;
		
	}

}
