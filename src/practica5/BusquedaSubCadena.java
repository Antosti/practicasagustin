import java.util.Scanner;

public class BusquedaSubCadena {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner cadena = new Scanner(System.in);
		String texto1 = " ";
		String texto2 = " ";
		
		System.out.println("Introduzca un parrafo: ");
		texto1 = cadena.nextLine();
		System.out.println("Introduzca la cadena que desea buscar dentro del parrafo: ");
		texto2 = cadena.nextLine();
		
		System.out.println("La cadena " + texto2 +" aparece " + obtenerNumeroVecesSubCadena(texto1, texto2) + " veces en el parrafo.");

	}
	
	
	public static int obtenerNumeroVecesSubCadena(String cadena1, String cadena2) {
		
		int contador = 0;
		
		while(cadena1.indexOf(cadena2) > -1) {
				cadena1 = cadena1.substring(cadena1.indexOf(cadena2) + cadena2.length(), cadena1.length());
				contador++;
			}
			
		
		
		return contador;
	}

}