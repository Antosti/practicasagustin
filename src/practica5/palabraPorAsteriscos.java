import java.util.Scanner;

public class palabraPorAsteriscos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner cadena = new Scanner(System.in);
		
		System.out.println("Escriba una texto: ");
		StringBuilder texto1 = new StringBuilder (cadena.nextLine());
		System.out.println("Escriba las palabras que desea sustituir por asteriscos sin espacios y separadas por comas: ");
		String texto2 = cadena.nextLine();
		
		System.out.println(reemplazaPalabras(texto1, texto2));

	}
	
	public static StringBuilder reemplazaPalabras(StringBuilder cadena, String cadena2) {
		
		
		String [] corte;
		int ind1 = 0;
		
		String asteriscos = "";
		
		corte = cadena2.split(",");
		
		for(int i = 0; i < corte.length; i++) {
			for(int j = 0; j < corte[i].length(); j++) {
				asteriscos += "*";
			}
			while(cadena.indexOf(corte[i]) > -1) {
				ind1 = cadena.indexOf(corte[i]);
				cadena.replace(ind1,ind1 + corte[i].length(), asteriscos);
			}
			asteriscos = "";
		}
		
		
		return cadena;
		
		
	}

}

