import java.util.Scanner;

public class Parentesis {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner cadena = new Scanner(System.in);
		String texto = "";
		
		System.out.println("Introduzca una ecuacion con parentesis para comprobar que el orden de los parentesis es el correcto: ");
		texto = cadena.nextLine();
		
		if(parentesisCorrectos(texto))
			System.out.println("El orden de los parentesis es el correcto");
		else
			System.out.println("El orden de los parentesis no es el correcto");

	}
	
	public static boolean parentesisCorrectos(String cadena) {
		
		int contador = 0;
		boolean sol = true;
		
		for(int i = 0; i < cadena.length(); i++) {
			
			if (cadena.charAt(i) == '(') 
				contador++;
				
			if (cadena.charAt(i) == ')')
				if (contador > 0)
					contador--;
				else
					sol = false;
				
		}
		
		if(contador != 0)
			sol = false;
		
		
		return sol;
		
	}

}
