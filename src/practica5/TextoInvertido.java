import java.util.Scanner;

public class TextoInvertido {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner cadena = new Scanner(System.in);
		String texto = "";
		
		System.out.println("Escriba un texto que desee invertir: ");
		texto = cadena.nextLine();
		
		System.out.println(texto + " -> " + invertirTexto(texto));

	}
	
	
	public static StringBuilder invertirTexto(String cadena) {
		
		StringBuilder invertida = new StringBuilder();
		int i = cadena.length()-1;
		
		do {
			invertida.append(cadena.charAt(i));
			i--;
			
		}while (i >= 0);
		
		
		return invertida;
		
	}

}
