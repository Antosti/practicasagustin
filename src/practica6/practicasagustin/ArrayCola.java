/**ArrayCola.java
 * Implementacion de una cola de tama�o din�mico utilizando arrays.
 * @author ACM
 * @date 27-2-2020
 */
package practicasagustin;


public class ArrayCola {
	private Object[] cola;
	private int cabeza;
	private int fin;
	
	public static void main(String[] args) {
		ArrayCola mensajes = new ArrayCola(3); 
		mensajes.offer("Hola");
		mensajes.offer("Que");
		mensajes.offer("Tal");
		Object[] test = mensajes.toArray();
		for (int i = 0; i < test.length; i++) {
			System.out.println(test[i]);
		}
		System.out.println("------------------------");
		mensajes.clear();
		mensajes.offer("Prueba");
		System.out.println("peek: "+ mensajes.peek());
		mensajes.offer("2");
		
		Object msg;
		while (mensajes.size() > 0) {
			msg = mensajes.poll();
			System.out.println(msg);
		}
		System.out.println(mensajes.size());
		
	}
		 
	/**
	 * Constructor vac�o
	 */
	public ArrayCola() {
		cola = new Object[5];
		cabeza=0;					//valor cuando no hay ningun elemento en la cola;
		fin=0;						//valor cuando no hay ningun elemento en la cola;
	}
	
	/**
	 * Constructor para no empezar la cola al comienzo del array
	 * @param indice
	 */
	public ArrayCola(int indice) {
		assert indice >= 0;
		cabeza = indice;
		fin = cabeza;
		cola = new Object[indice + 5];
	}

	/**
	 * Duplica el espacio de la cola
	 */
	public void duplicarEspacio() {
		Object tempArray[] = new String[cola.length*2];
		tempArray = cola;
		cola = new String[tempArray.length];
	}
	
	/**
	 * Duplica el espacio de la pila si esta llena
	 */
	public void checkSize() {
		if(fin+1 == cola.length) {
			duplicarEspacio();
		}
	}

	/**
	 * Elimina todos los elementos de la cola
	 */
	public void clear() {
		for (int i = cabeza; i <= fin; i++) {
			cola[i]=null;
		}
		cabeza=0;
		fin =cabeza;
	}
	
	/**
	 * Comprueba si el elemento est� en la cola
	 * @param objeto
	 * @return
	 */
	public boolean contains(Object objeto) {
		assert !isEmpty();
		for (int i = cabeza; i < fin; i++) {
			if(cola[i].equals(objeto)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Comprueba si la cola est� vac�a
	 * @return true-si est� vac�a ,false-si no est� vac�a
	 */
	public boolean isEmpty() {
		if(cabeza == fin && cola[cabeza]==null) {
			return true;
		}
		return false;
	}
	
	/**
	 * Obtiene el �ltimo elemento de la cola sin eliminarlo
	 * @return El �ltimo elemento de la cola
	 */
	public Object peek() {
		assert !isEmpty();
		return cola[cabeza];
	}
	
	
	public Object poll() {
		Object salida = cola[cabeza];
		cola[cabeza] = null;
		if (cabeza<fin) {
		cabeza ++;
		}
		return salida;
		
	}
	
	/**
	 * Devuelve el n�mero de elementos almacenados
	 * @return Cantidad de elementos en la cola
	 */
	public int size() {
		if(isEmpty()) {
			return 0;
		}
		return fin - cabeza + 1;
	}
	/**
	 * A�ade elemento al final de la cola
	 * @param dato
	 */
	public void offer(Object dato) {
		checkSize();
		if (isEmpty()) {
			cola[cabeza]=dato;
		}
		else {
			fin++;
			cola[fin] = dato;
		}
		
		
	}

	/**
	 * Devuelve un array con los elementos de la pila
	 * @return salida
	 */
	public Object[] toArray() {
		assert !isEmpty();
		Object[] salida = new Object[size()];
		for (int i = 0; i < size(); i++) {
			salida[i]=cola[cabeza+i];			
		}
		return salida;
		
	}

}
