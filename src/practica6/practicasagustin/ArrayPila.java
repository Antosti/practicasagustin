/**ArrayPila.java
 * Implementacion de una pila de tama�o din�mico utilizando arrays.
 * @author ACM
 * @date 27-2-2020
 */
package practicasagustin;

import java.util.Arrays;

public class ArrayPila {
	Object[] pila; 
	private int top;	//posici�n del �ltimo elemento
	
	public static void main(String[] args) {
		ArrayPila pila = new ArrayPila();
		pila.push("1. Camelia");
		pila.push("2. Azalea");
		pila.push("3. Jazmin");
		pila.push("4. Datura");
		Object[] plantas = pila.toArray();
		System.out.println("Cima de la pila: " + pila.peek());
		while (pila.size() > 0) {
		Object nombrePlanta = pila.pop();
		System.out.println(nombrePlanta);
		}
		//Muestra el array de nombres de plantas. La pila ya est� vac�a.
		System.out.println("Array:\n" + Arrays.toString(plantas));

		
	}

	public ArrayPila() {
		pila = new Object[5];
		top = -1;				//valor cuando no hay ningun elemento en la pila;
	}

	/**
	 * Duplica el espacio de la pila
	 */
	public void duplicarEspacio() {
		Object tempArray[] = new String[pila.length*2];
		tempArray = pila;
		pila = new String[tempArray.length];
	}
	
	/**
	 * Duplica el espacio de la pila si esta llena
	 */
	public void checkSize() {
		if(top+1 == pila.length) {
			duplicarEspacio();
		}
		
	}
	/**
	 * A�ade un elemento en la parte superior de la pila
	 * @param dato
	 */
	public void push(Object dato) {
		checkSize();
		top++;
		pila[top]=dato;
	}
	/**
	 * Extrae el elemento existente en la parte superior de la pila(lo borra)
	 */
	public Object pop() {
		assert !isEmpty();
		Object salida = pila[top];
		pila[top] = null;
		top--;
		return salida;
		
	}
	/**
	 * Elimina todos los elementos de la lista
	 */
	public void clear() {
		for (int i = 0; i < top; i++) {
			pila[i]=null;
		}
		top = -1;
	}
	
	/**
	 * Comprueba si el elemento est� en la lista
	 * @param objeto
	 * @return
	 */
	public boolean contains(Object objeto) {
		assert !isEmpty();
		for (int i = 0; i < top; i++) {
			if(pila[i].equals(objeto)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Comprueba si la pila est� vac�a
	 * @return true-si est� vac�a ,false-si no est� vac�a
	 */
	public boolean isEmpty() {
		if(top < 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * Obtiene el �ltimo elem,ento de la pila sin eliminarlo
	 * @return El �ltimo elemento de la pila
	 */
	public Object peek() {
		assert !isEmpty();
		return pila[top];
	}
	/**
	 * Devuelve el n�mero de elementos almacenados
	 * @return Cantidad de elementos en la pila
	 */
	public int size() {
		return top + 1;
	}
	
	/**
	 * Devuelve un array con los elementos de la pila
	 * @return salida
	 */
	public Object[] toArray() {
		assert !isEmpty();
		Object[] salida = new Object[size()];
		for (int i = 0; i < size(); i++) {
			salida[i]=pila[i];			
		}
		return salida;
		
	}
	
	@Override
	public String toString() {
		return "ArrayPila [pila=" + Arrays.toString(pila) + "]";
	}
	
}
