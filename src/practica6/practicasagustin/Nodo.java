/**Nodo.java
 * Representa la implementaci�n m�s sencilla y b�sica de los nodods que forma una lista
 * @author ACM
 * @date 27-2-2020
 */
package practicasagustin;



class Nodo {
		// Atributos
		Object dato;
		Nodo siguiente;

		/**
		 * Constructor que inicializa atributos por defecto.
		 * 
		 * @param elem - el elemento de informaci�n �til a almacenar.
		 */
		public Nodo(Object dato) {
			this.dato = dato;
			siguiente = null;
		}

	} // class