/**PruebaListaEnlazada.java
 * Clase para probar la funcionalidad de la clase ListaEnlazada
 * @author ACM
 * @date 27-2-2020
 */
package practicasagustin;

public class PruebaListaEnlazada {
	public static void main(String[] args) {
		ListaEnlazada listaCompra = new ListaEnlazada();
		ListaEnlazada listaBorrar = new ListaEnlazada();
		listaBorrar.add("Leche");
		listaBorrar.add("Miel");
		listaCompra.add("Leche");
		listaCompra.add("Miel");
		listaCompra.add("Aceitunas");
		listaCompra.add(2,"Tercero");
		listaCompra.add("Cerveza");
		listaCompra.add(0,"Primero");
		listaCompra.add(listaCompra.size()+1,"�ltimo");
		listaCompra.add("Caf�");
		//listaCompra.removeAll(listaBorrar);
		System.out.println("Lista de la compra:");
		for (int i = 0; i < listaCompra.size(); i++) {
			System.out.println(listaCompra.get(i));
		}
		
		System.out.println("elementos en la lista: " + listaCompra.size());
		System.out.println("elementos 3 en la lista: " + listaCompra.get(3));
		System.out.println("posici�n del elemento Caf�: " + listaCompra.indexOf("Caf�"));
		System.out.println("eliminado: " + listaCompra.remove("Caf�"));
		
		listaCompra.retainAll(listaBorrar);
		System.out.println("Lista de la compra:");
		for (int i = 0; i < listaCompra.size(); i++) {
			System.out.println(listaCompra.get(i));
		}
		
		listaCompra.addAll(listaBorrar);
		System.out.println("Lista de la compra:");
		for (int i = 0; i < listaCompra.size(); i++) {
			System.out.println(listaCompra.get(i));
		}
	}
}
