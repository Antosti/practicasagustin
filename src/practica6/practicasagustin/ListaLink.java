/*ListaLink.java
 * Clase de una lista enlazada simple.
 * 
 * @author ACM 2020
 */

package practicasagustin;
/**
 * Representa la implementación parcial de una lista enlazada simple en la que
 * se tiene acceso directo al primer elemento y también al último.
 */
public class ListaLink {

    /**
     * La clase Nodo está anidada y representa la estructura de un elemento de
     * la lista enlazada simple.
     */
	
	public static void main(String[] args) {
		
		ListaLink test = new ListaLink();
		test.add(0, 11);
		test.addUltimo(27);
		test.add(0, 15);
		test.addUltimo("Hola");
		test.add(1,2);
		
		leerLista(test, test.getNumeroElementos());
		
		
	}
	
	
    class Nodo {

        //Atributos
        Object dato;
        Nodo siguiente;

        /**
         * Constructor que inicializa atributos al valor por defecto.
         */
        public Nodo() {
            dato = null;
            siguiente = null;
        }

    } //class Nodo


    //Atributos ListaLink
    private  Nodo primero;
    private  Nodo ultimo;
    private int numElementos;

    /**
     * Constructor que inicializa los atributos al valor por defecto
     */
    public ListaLink() {
        primero = null;
        ultimo = null;
        numElementos = 0;
    }
    
    public void add(int i, Object dato) {
    	
	    	if(dato != null) {
	    	Nodo nuevo;
	    	Nodo aux = new Nodo();
	    	nuevo = new Nodo();
	    	nuevo.dato = dato;
	    	
	    	if(numElementos == 0) {
	    		primero = nuevo;
	    		ultimo = nuevo;
	    		numElementos++;
	    		}
	    	else if (i == 0) {
	    			nuevo.siguiente = primero;
	    			primero = nuevo;
	    			numElementos++;
	    		}
	    	else if (i < numElementos-1 && i > 0) {
	    		
	    		aux = buscaNodoAnterior(i);
	    		nuevo.siguiente = aux.siguiente.siguiente;
	    		aux.siguiente.siguiente = nuevo;
	    		numElementos++;
	    		
	    		
	    	}
	    	else
	    		System.out.println("La posicion " + i + " no es valida");
	    	
	    }
    }
    
    public int getNumeroElementos()
    {
    	return numElementos;
    }
    
    public Nodo buscaNodoAnterior(int i) {
    	Nodo aux = new Nodo();
    	aux.siguiente = primero;
		for (int j = 0; j < numElementos; j++) {
		    		
		    		if (j+1 == i) {
		    			return aux;
		    	}
		    		aux.siguiente = aux.siguiente.siguiente;
		    			
		    	}
		return aux;
    }
    
    public void addUltimo(Object dato) {
    	
    	Nodo nuevo = new Nodo();
    	Nodo aux = new Nodo();
    	nuevo.dato = dato;
    	
    	aux = buscaNodoAnterior(numElementos);
    	aux.siguiente.siguiente = nuevo;
    	ultimo = nuevo;
    	numElementos++;
    	
    }
    
    static private void leerLista(ListaLink lista, int size) {
		for (int i = 1; i < size+1; i++) {
			System.out.println(lista.buscaNodoAnterior(i).siguiente.dato);
		}
		System.out.println("primero:" + lista.primero.dato + " ultima:" + lista.ultimo.dato);
	}
    
    

} //class listaLink
