/**ListaEnlazada.java
 * Representa la implementaci�n m�s sencilla y b�sica de una lista enlazada simple
 * con acceso s�lo al principio de la serie de nodos, utiliza la clase Nodo.java
 * @author ACM
 * @date 27-2-2020
 */
package practicasagustin;

public class ListaEnlazada {
// Atributos
	private Nodo primero; // Referencia a nodo
	private int numElementos;

// M�todos
	/**
	 * Constructor que inicializa los atributos al valor por defecto. Lista vac�a.
	 */
	public ListaEnlazada() {
		primero = null;
		numElementos = 0;
	}


	/**
	 * A�ade un elemento al final de la lista.
	 * 
	 * @param elem - el elemento a a�adir. Admite que el elemento a a�adir sea null.
	 */
	public void add(Object dato) {
		// variables auxiliares
		Nodo nuevo = new Nodo(dato);
		Nodo ultimo = null;
		if (numElementos == 0) {
			// Si la lista est� vac�a enlaza el nuevo nodo el primero.
			primero = nuevo;
		} else {
			// Obtiene el �ltimo nodo y enlaza el nuevo.
			ultimo = obtenerNodo(numElementos - 1);
			ultimo.siguiente = nuevo;
		}
		numElementos++; // Actualiza el n�mero de elementos.
	}

	/**
	 * Obtiene el nodo correspondiente al �ndice. Recorre secuencialmente la cadena
	 * de enlaces. * @param indice - posici�n del nodo a obtener.
	 * 
	 * @return - el nodo que ocupa la posici�n indicada por el �ndice.
	 */
	private Nodo obtenerNodo(int indice) {
		assert indice >= 0 && indice < numElementos;
// Recorre la lista hasta llegar al nodo  de posici�n buscada.
		Nodo actual = primero;
		for (int i = 0; i < indice; i++)
			actual = actual.siguiente;
		return actual;
	}

	/**
	 * Elimina el elemento indicado por el �ndice. Ignora �ndices negativos
	 * 
	 * @param indice - posici�n del elemento a eliminar
	 * @return - el elemento eliminado o null si la lista est� vac�a.
	 * @exception IndexOutOfBoundsException - �ndice no est� entre 0 y
	 *                                      numElementos-1
	 */
	public Object remove(int indice) {
// Lanza excepci�n si el �ndice no es v�lido.
		if (indice >= numElementos || indice < 0) {
			throw new IndexOutOfBoundsException("�ndice incorrecto: " + indice);
		}
		if (indice > 0) {
			return removeIntermedio(indice);
		}
		if (indice == 0) {
			return removePrimero();
		}
		return null;
	}

	/**
	 * Elimina el primer elemento.
	 * 
	 * @return - el elemento eliminado o null si la lista est� vac�a.
	 */
	private Object removePrimero() {
		// variables auxiliares
		Nodo actual = null;
		actual = primero; // Guarda actual.
		primero = primero.siguiente; // Elimina elemento del principio.
		numElementos--;
		return actual.dato;
	}

	/**
	 * Elimina el elemento indicado por el �ndice.
	 * 
	 * @param indice - posici�n del elemento a eliminar.
	 * @return - el elemento eliminado o null si la lista est� vac�a.
	 */
	private Object removeIntermedio(int indice) {
		assert indice > 0 && indice < numElementos;
		// variables auxiliares
		Nodo actual = null;
		Nodo anterior = null;
		// Busca nodo del elemento anterior correspondiente al �ndice.
		anterior = obtenerNodo(indice - 1);
		actual = anterior.siguiente; // Guarda actual.
		anterior.siguiente = actual.siguiente; // Elimina el elemento.
		numElementos--;
		return actual.dato;
	}

	/**
	 * Elimina el dato especificado.
	 * 
	 * @param dato � a eliminar.
	 * @return - el �ndice del elemento eliminado o -1 si no existe.
	 */
	public int remove(Object dato) {
		// Obtiene el �ndice del elemento especificado.
		int actual = indexOf(dato);
		if (actual != -1) {
			remove(actual); // Elimina por �ndice.
		}
		return actual;
	}

	/**
	 * Busca el �ndice que corresponde a un elemento de la lista.
	 * 
	 * @param dato- el objeto elemento a buscar.
	 */
	public int indexOf(Object dato) {
		Nodo actual = primero;
		for (int i = 0; actual != null; i++) {
			if ((actual.dato != null && actual.dato.equals(dato)) || actual.dato == dato) {
				return i;
			}
			actual = actual.siguiente;
		}
		return -1;
	}

	/**
	 * @param indice � obtiene un elemento por su �ndice.
	 * @return elemento contenido en el nodo indicado por el �ndice.
	 * @exception IndexOutOfBoundsException - �ndice no est� entre 0 y
	 *                                      numElementos-1.
	 */
	public Object get(int indice) {
// lanza excepci�n si el �ndice no es v�lido
		if (indice >= numElementos || indice < 0) {
			throw new IndexOutOfBoundsException("�ndice incorrecto: " + indice);
		}
		Nodo aux = obtenerNodo(indice);
		return aux.dato;
	}
	/**
	 * Inserta elementos en la estructura.
	 * Interpreto que "insertar" es a�adir elementos entre el primero y el �ltimo
	 * @param indice
	 * @param dato
	 */
	public void add(int indice,Object dato) {
		if(indice == 0 || indice == numElementos+1) {
			System.out.println("�ste m�todo solo insereta entre el primero y el �ltimo");
			return;
		}
		Nodo nuevoNodo = new Nodo(dato);
		nuevoNodo.siguiente = obtenerNodo(indice-1).siguiente;
		obtenerNodo(indice-1).siguiente = nuevoNodo;
		numElementos++;
	}
	
	/**
	 * Elimina todos los elementos de la lista
	 * @param datosAborrar
	 */
	public void removeAll(ListaEnlazada datosAborrar) {
		for (int i = 0; i < numElementos; i++) {
			if (buscarCoincidencia(get(i),datosAborrar)) {
				if(obtenerNodo(i)==primero) {
					removePrimero();
				}
				else {
					removeIntermedio(i);
				}
				i--;
			}
		}
	}
	/**
	 * M�todo creado para la implementacion de removeAll(ListaEnlazada)
	 * Busca coincidencias de unb Nodo una lista, devuelve true si la encuentra, false si no
	 * @param nodo
	 * @param lista
	 * @return
	 */
	public boolean buscarCoincidencia(Object nodo,ListaEnlazada lista) {
		for (int i = 0; i < lista.size(); i++) {
			if(lista.get(i) == nodo) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Elimina todos los elementos que no est�n en la lista obtenida como par�metro
	 * @param lista
	 */
	public void retainAll(ListaEnlazada lista) {
		for (int i = 0; i < numElementos; i++) {
			if (!buscarCoincidencia(get(i),lista)) {
				if(obtenerNodo(i)==primero) {
					removePrimero();
				}
				else {
					removeIntermedio(i);
					
				}
				i--;
			}
		}
	}
	
	/**
	 * Añade todos los elementos de una lista al final
	 * @param lista
	 */
	public void addAll(ListaEnlazada lista) {
		for (int i = 0; i < lista.size(); i++) {
			add(lista.get(i));
			}
	}
	
	/**
	 * @return el n�mero de elementos de la lista
	 */
	public int size() {
		return numElementos;
	}

} // class
