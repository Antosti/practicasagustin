/*ConstruirArbol2.java
 * Programa que dibuja una piramide con asteriscos
 * ACM 2019
 */

import java.util.Scanner;

public class ConstruirArbol2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		int num = 0;
		String piramide = "";
		
		System.out.print("Introduzca un numero para construir la piramide: ");
		num = entero.nextInt();
		piramide = piramide2(num);
		System.out.println(piramide);
	}
	
	static String piramide2(int num) {
		String solucion = "";
		int aux = num+1;
		num += 1;
		
		for (int i = 1; i < num; i++) {
			
			for (int j = 2; j < aux; j ++) {
				solucion += " ";
			}
			for (int a = 1; a <= ((i * 2)-1); a++) {
				solucion += "*";
			}
			solucion += "\n";
			aux--;
			
		}
		return solucion;
	}

}
