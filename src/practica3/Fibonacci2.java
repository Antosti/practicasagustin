/*Fibonacci2.java
 * Programa que calcula el valor de la posicion dentro de la serie de fibonacci
 * ACM 2019
 */

import java.util.Scanner;

public class Fibonacci2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		
		int num = 0;
		int res = 0;
		
		System.out.print("Escriba la posicion de la serie fibonacci que desee conocer: ");
		num = entero.nextInt();
		
		res = FibonacciA(num);
		System.out.println("El valor de la posicion " + num + " es " + res);

	}
	
	static int FibonacciA(int a) {
		
		assert a >= 0;
		int valor = 1;
		int aux = 0;
		int numant = 0;
	
		if (a == 0)
			valor = 0;
		else {
			for (int i = 2; i <= a; i++) {
				aux = valor;
				valor += numant;
				numant = aux;
			}
		}
		
		return valor;
		
	}

}
