/*ConstruirArbol.java
 * Programa que dibuja una piramide con asteriscos
 * ACM 2019
 */

import java.util.Scanner;

public class ConstruirArbol {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		int num = 0;
		
		System.out.print("Introduzca un numero para construir la piramide: ");
		num = entero.nextInt();
		piramide(num);

	}
	
	static void piramide(int num) {
		
		int aux = num+1;
		num += 1;
		
		for (int i = 1; i < num; i++) {
			
			for (int j = 2; j < aux; j ++) {
				System.out.print(" ");
			}
			for (int a = 1; a <= ((i * 2)-1); a++) {
				System.out.print("*");
			}
			System.out.println();
			aux--;
			
		}
		
		
	}

}
