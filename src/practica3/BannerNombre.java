/*BannerNombre.java
 * Programa que dibuja las iniciales de un nombre por pantalla
 * ACM 2019
 */

public class BannerNombre {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		banner();
	}
	static void banner() {
		System.out.println("" +
				"                                A A     CCCCCCC  M     M\r\n" + 
				"                               A   A    C        MM   MM\r\n" + 
				"                              A     A   C        M M M M\r\n" + 
				"                              AAAAAAA   C        M  M  M\r\n" + 
				"                              A     A   C        M     M\r\n" + 
				"                              A     A   C        M     M\r\n" + 
				"                              A     A   CCCCCCC  M     M");
	}
}
