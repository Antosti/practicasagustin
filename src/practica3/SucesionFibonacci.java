/*SucesionFibonacci.java
 * Programa que te devuelve el valor de la posicion de la secuencia de fibonacci
 * ACM 2019
 */

import java.util.Scanner;

public class SucesionFibonacci {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		
		int num = 0;
		int res = 0;
		
		System.out.print("Escriba la posicion de la serie fibonacci que desee conocer: ");
		num = entero.nextInt();
		
		while (num < 0) {
			System.out.println("Numero no valido. Introduzca un valor mayor o igual a 0: ");
			num = entero.nextInt();
		}
		res = fibonacci(num);
		System.out.println("La posicion "+ num + " de la serie fibonacci tiene un valor de "+res);

	}
	
	static int fibonacci(int num) {
		int valor = 1;
		int aux = 0;
		int numant = 0;
		
		if (num == 0)
			valor = 0;
		else {
			for (int i = 2; i <= num; i++) {
				aux = valor;
				valor += numant;
				numant = aux;
			}
		}
		
		return valor;
	}
	
}
