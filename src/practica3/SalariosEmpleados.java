/*SalariosEmpleados.java
 * Programa que calcula el salario de los empleados y su salario extra segun las horas trabajadas
 * ACM 2019
 */


import java.util.Scanner;

public class SalariosEmpleados {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		
		int horas = 1;
		int salario = 0;
		
		System.out.println("Introduzca el numero de horas realizadas por el trabajador");
		System.out.println("Pulse 0 para salir");
		
		while (horas != 0) {
			
			System.out.print("\n\nHoras trabajadas por el empleado: ");
			horas = entero.nextInt();
			if (horas != 0) {
				salario = calcularSalario(horas);
				System.out.println("Salario del empleado: " + salario);
			}
					
		}

	}
	
	static int calcularSalario(int horas) {
		
		int salario = 0;
		int extra = 0;
		
		if (horas > 35) {
			extra = horas - 35;
			extra *= 22;
			salario = (35 * 15) + extra;
		}else
			salario = horas * 15;
		
		
		return salario;
	}

}
