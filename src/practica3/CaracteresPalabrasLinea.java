/*CaracteresPalabrasLineas
 * Programa que te cuenta los caracteres, las palabras y las lineas de un texto y las codifica
 * ACM 2019
 */

import java.util.Scanner;

public class CaracteresPalabrasLinea {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner palabra = new Scanner (System.in);
		Scanner entero = new Scanner (System.in);
		Scanner letra = new Scanner (System.in);
		int num = 1;
		String texto = "";
		String s = "";
		
		do {
			num = 1;
			System.out.println("Escriba el texto que desee codificar: ");
			texto = palabra.nextLine();
			while (num != 0) {
				System.out.println("Elija que parte desea consultar:\nPulse 1 para ver el total de caracteres\nPulse 2 para ver el total"
						+ " de palabras\nPulse 3 para ver el total de lineas\nPulse 0 para codificar otro texto o salir.");
				num = entero.nextInt();
				InterpretarCodigo(texto, num);
			}
			
			System.out.println("Si desea codificar otro texto pulse s. Para salir "
					+ "pulse cualquier otra letra: ");
			s = letra.next();
		}while (comprobarLetra(s));

	}
	
	static String contarCaracteresPalabrasLineas(String texto) {
		String solucion = "";
		String[] palabras;
		String[] palabras2;
		String[] lineas;
		char[] caracteres;
		int caracter = 0;
		int palabra = 0;
		int linea = 0;
		
		palabras = texto.split(" ");
		palabras2 = texto.split("\\\n");
		lineas = texto.split("\\\\n");
		caracteres = texto.toCharArray();
		caracter = caracteres.length;
		palabra = palabras.length + palabras2.length;
		linea = lineas.length;
		solucion += caracter + "-" + palabra + "-" + linea;
						
		return solucion;
	}
	
	static void InterpretarCodigo(String texto, int num) {
		
		String codigo = contarCaracteresPalabrasLineas(texto);
		String[] solucion = codigo.split("-");
		
		switch(num){
			case 1:
				System.out.println("El texto tiene " +  solucion[0] + " caracteres.");
				break;
			case 2:
				System.out.println("El texto tiene " +  solucion[1] + " palabras.");
				break;
			case 3:
				System.out.println("El texto tiene " +  solucion[2] + " lineas.");
				break;
		}
		
	}
	
	static boolean comprobarLetra(String c) {
		if(c.equalsIgnoreCase("s"))
			return true;
		else 
			return false;
	}

}
