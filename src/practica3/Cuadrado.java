/*Cuadrado.java
 * Programa que crea un cuadrado, usando de contorno un caracter introducido por el usuario
 * ACM 2019
 */

import java.util.Scanner;

public class Cuadrado {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		Scanner letra = new Scanner (System.in);
		
		int num = 0;
		char a = ' ';
		
		System.out.print("Introduzca un numero para el tama�o del cuadrado: ");
		num = entero.nextInt();
		System.out.print("Introduzca el caracter con el que dibujar el cuadrado: ");
		a = letra.next().charAt(0);
		while (!numeroValido(num)) {
			System.out.print("No se puede realizar un cuadrado de ese tama�o. Pruebe con un numero mayor que 0: ");
			num = entero.nextInt();
		}
			muestraCuadrado(num, a);

			
		

	}
	static void muestraCuadrado(int i, char a) {
		
		for (int c = 0; c < i; c++)
		{
			System.out.print(a);
		}
		System.out.println();
		
		for (int c = 0; c < i -2; c++)
		{
			System.out.print(a);
			for (int j = 0; j < i -2; j++)
			{
				System.out.print(" ");
			}
			System.out.println(a);
		}
		
		for (int c = 0; c < i; c++)
		{
			System.out.print(a);
		}
		
	}
	
	static boolean numeroValido (int n) {
		if (n > 0)
			return true;
		else
			return false;
	}

}
