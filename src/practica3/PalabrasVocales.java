/*PalabrasVocales.java
 * Programa que muestra en una lista, todas las palabras con 4 vocales de un texto
 * ACM 2019
 */


import java.util.Scanner;
import java.util.Vector;

public class PalabrasVocales {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner palabra = new Scanner (System.in);
		Vector <String> solucion = new Vector<String>();
		
		
		System.out.println("Escriba las palabras que quiera separadas por espacios, y"
				+ " el programa devolvera las palabras con cuatro vocales distintas: ");
		String a = palabra.nextLine();
		
		solucion = PalabrasCuatroVocales(a);
		System.out.println(solucion);
			
	

	}
	
	static Vector <String> PalabrasCuatroVocales (String a) {
		
		String[] b;
		char[] x;
		Vector<Character> y = new Vector<Character>();
		Vector<String> z = new Vector<String>();
		int valido = 0;
		
		b = a.split(" ");
		
		for (int i = 0; i< b.length; i++)
		{
			
			String c = b[i];
			x = c.toCharArray();
			y.clear();
			for (int j = 0; j < x.length; j++) {
				
				 if (comprobarVocales(x[j])) {
					if(y.isEmpty())
						y.add(x[j]);
					else {
						for(int k = 0; k < y.size();k++)
						{
							if (y.contains(x[j]))
								valido++;
						}
						if (valido < 1)
							y.add(x[j]);
					}
					valido = 0;
				}
				 
			}
			if (y.size() > 3)
				 z.add(c);

			
		}
		
		
		
		return z;
	}
	
	static boolean comprobarVocales(char a) {
		if (a == 'a' || a == 'A' || a == 'e' || a == 'E' || a == 'i' 
				|| a == 'I' || a == 'o' || a == 'O' || a == 'u' 
				|| a == 'U')
		{
			return true;
		}else
			return false;
		
	}

}
