/*ConstruirRombo.java
 * Programa que dibuja una rombo con asteriscos
 * ACM 2019
 */

import java.util.Scanner;

public class ConstruirRombo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		int rombo = 0;
		
		System.out.print("Introduzca un numero impar para realizar el rombo: ");
		rombo = entero.nextInt();
		while ((rombo%2) == 0){
			System.out.print("Numero no valido, introduzca un numero impar: ");
			rombo = entero.nextInt();			
		}
		Rombo(rombo);

	}
	
	static void Rombo(int num) {
		int aux = num+1;
		double num2 = num;
		num += 1;
		int aux2 = 0;
		
		
		for (int i = 1; i < (num/2)+1; i++) {
			
			if (i <= ((num2/2)+1)){
				for (int j = 2; j < aux; j ++) {
					System.out.print(" ");
				}
				for (int a = 1; a <= ((i * 2)-1); a++) {
					System.out.print("*");
				}
				System.out.println();
				aux--;
			}		
		}
		aux += 2;
		for (int i = 1; i < (num/2); i++) {
			for (int j = 2; j < aux; j ++) {
				System.out.print(" ");
			}
			for (int a = 2; a <= (num - (2*i)); a++) {
				System.out.print("*");
			}
			System.out.println();
			aux++;
		}
		
	}

}
