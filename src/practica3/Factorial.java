/*Factorial.java
 * Programa que calcula el factorial de un numero entero mayor que 0
 * ACM 2019
 */


import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		int num = 0;
		int factorial = 0;
		
		System.out.print("Introduzca un numero para calular su factorial: ");
		num = entero.nextInt();
		while (num < 0) {
			System.out.print("Valor no valido, introduzca un numero positivo: ");
			num = entero.nextInt();
		}
		factorial = calcularFactorial(num);
		System.out.println("El factorial de "+ num + " es " + factorial);
		
		

	}
	
	static int calcularFactorial(int num) {
		
		int resultado = 0;
		if (num == 0)
			resultado = 1;
		else {
			resultado = num;
			for (int i = num -1; i > 0; i--)
			{
				resultado *=  i;
			}		
		}
		return resultado;
	}

}

