/*Ecuaciones.java
 * Programa que resuelve ecuaciones de segundo grado
 * ACM 2019
 */

import java.util.Scanner;

public class Ecuaciones {

	static double discriminante;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner doble = new Scanner (System.in);
		double a = 0;
		double b = 0;
		double c = 0;
		double x1 = 0;
		double x2 = 0;
		double y = 0;
		double x = 0;
		
		System.out.println("Este programa resuelve ecuaciones de segundo grado. Introduzca los valores de a, b y c de la ecuacion: ");
		a = doble.nextDouble();
		b = doble.nextDouble();
		c = doble.nextDouble();
		
		switch(evaluarDiscriminante(a,b,c)) {
		case 1:
			System.out.println("La ecuacion es degenerada.");
			break;
		case 2:
			x = -c/b;
			System.out.println("La raiz unica es " + x);
			break;
		case 3:
			x = Math.rint((-b/(2*a))*100)/100;
			y = Math.rint((Math.sqrt((Math.pow(b, 2) - (4*a*c))) / (2*a))*100)/100;
			x1 = x + y;
			x2 = x - y;
			System.out.println("Las dos raices reales de la ecuacion son: " + x1 + " y " + x2);
			break;
		case 4:
			x = -b/(2*a);
			y = Math.rint((Math.sqrt(-(Math.pow(b, 2) - (4*a*c)) ) / (2*a)) *100)/100;
			
			System.out.println("Las dos raices complejas de la ecuacion son:\n " + x + " +  " + y + "i\n" + x + " - " + y + "i");
			break;
		}
	doble.close();
	

	}
	
	static int evaluarDiscriminante(double a, double b, double c) {
		
		discriminante = Math.pow(b, 2)  - (4*a*c);
		if(a == 0 && b == 0)
			return 1;
		else if(a == 0 && b != 0)
				return 2;
		else if (discriminante >= 0)
			return 3;
		else
			return 4;
		
		
		
	}
}
