/*TablasMultiplicar.java
 * Programa que muestra los primeros 15 productos de las tablas de multiplicar de un numero
 * ACM 2019
 */

import java.util.Scanner;

public class TablasMultiplicar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		
		
		System.out.print("Introduzca un numero para sacar su tabla de multiplicacion: ");
		calcularMostrarTabla(entero.nextInt());
		
		
	}
	
	static void calcularMostrarTabla(int num) {
		System.out.println("Tabla de multiplicar del numero " + num);
		
		for (int i = 0; i < 15; i ++) {
			System.out.println((i+1) + ". " + num + " * " + i + " = " + (i*num));
		}
		
	}

}
