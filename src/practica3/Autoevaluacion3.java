/*Autoevaluacion3.java
 * Programa que genera una barra de caracteres en base a un numero
 * ACM 2019
 */

import java.util.Scanner;

public class Autoevaluacion3 {

	final static int LIMITE_SUPERIOR = 10;
	final static int LIMITE_INFERIOR = -10;
	final static char CARACTER_BASE = '*';
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entero = new Scanner (System.in);
		int a = 0;
		String s = "";
		System.out.println("Escriba numeros para generar la barra con caracteres");
		System.out.println("Pulse 100 para salir");

		while(a != 100) {
			a = entero.nextInt();
			if(a==100)
				break;
			System.out.println(generarBarraHorizontal(a));
		}
		
		
		
		
		
	}
	static String generarBarraHorizontal(int num) {
		String solucion = "";
		solucion += (num + "\t");
		if (!comprobarNumero(num))
			solucion += "\tFUERA DE RANGO";
		else {
			if (num < 0) {
				int aux = LIMITE_INFERIOR - num;
				for (int i = 0; i > aux; i--) {
					solucion += " ";
				}
				solucion += generarBloquesCaracteres(num, CARACTER_BASE);
				solucion += "|";
			}
			else {
				for (int i = 0; i < 10; i++){
					solucion += " ";
				}
				solucion += "|";
				solucion += generarBloquesCaracteres(num, CARACTER_BASE);
			}
		}
		
		
		
		return solucion;
	}
	
	static String generarBloquesCaracteres(int num, char c) {
		String solucion = "";
		if (num > 0) {
			for (int i = 0; i < num; i++) {
				solucion += c;
			}
		}
		else {
			for (int i = 0; i > num; i--) {
				solucion += c;
			}
		}
		
		return solucion;
	}
	
	static boolean comprobarNumero(int num) {
		if (num < -10 || num > 10)
			return false;
		else
			return true;
	}
	

}
