/*Triangulo.java
 * Programa que muestra el tipo de triangulo que es y si es posible construirlo dado el tama�o de los lados del triangulo
 *  ACM 2019
 */

import java.util.Scanner;

public class Triangulo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner doble = new Scanner (System.in);
		double a;
		double b;
		double c;
		
		System.out.println("Introduzca el tamaño de los lados de un triangulo: ");
		System.out.print("Lado 1: ");
		a = doble.nextInt();
		System.out.print("Lado 2: ");
		b = doble.nextInt();
		System.out.print("Lado 3: ");
		c = doble.nextInt();
		
		if (esTriangulo(a,b,c))
			System.out.println(tipoTriangulo(a,b,c));
		else
			System.out.println("Error");

	}
	
	static boolean esTriangulo(double a, double b, double c) {
		
		if ((a + b) <= c || (a + c) <= b || (b + c) <= a)
			return false;
		else
			return true;
	}
	
	static String tipoTriangulo(double a, double b, double c) {
		String sol = "";
		
		if (a != b && a != c && b != c)
			sol += "El triangulo es de tipo escaleno.";
		else if (a == b && b == c)
			sol += "El triangulo es equilatero.";
		else 
			sol += "El triangulo es isosceles.";
		
		return sol;
	}

}
